package command

import (
	"fmt"
	"io"
	"sort"
	"strconv"
	"text/tabwriter"
	"time"

	"github.com/andygrunwald/go-jira"
	"github.com/fatih/color"
	"github.com/urfave/cli"
)

type issueData struct {
	issueId  string
	duration time.Duration
}

// CmdMain summarizes the tickets the user has recently tracked time against
func CmdMain(c *cli.Context) error {
	if c.NArg() != 1 && c.NArg() != 0 {
		return cli.NewExitError("Usage \"jira-standup {date}\"", 1)
	}

	username := c.String("username")
	if username == "" {
		return cli.NewExitError("You must specify --username", 1)
	}

	password := c.String("password")
	if password == "" {
		return cli.NewExitError("You must specify --password", 1)
	}

	url := c.String("url")
	if url == "" {
		return cli.NewExitError("You must specify --url", 1)
	}

	date, err := handleDate(c.Args().Get(0))
	if err != nil {
		return err
	}

	client, err := getClient(url, username, password)
	if err != nil {
		return fmt.Errorf("Unable to get client: %v", err)
	}

	return printDurations(client, username, date, c.App.Writer)
}

func printDurations(client *jira.Client, username string, date time.Time, writer io.Writer) error {
	durations, err := getDurations(client, username, date)
	if err != nil {
		return fmt.Errorf("Unable to get durations: %v", err)
	}

	sort.Slice(durations, func(i, j int) bool {
		return durations[i].duration < durations[j].duration
	})
	total := time.Duration(0)

	green := color.New(color.FgGreen).SprintFunc()
	tabW := tabwriter.NewWriter(writer, 0, 0, 1, ' ', 0)
	for _, issue := range durations {
		fmt.Fprintf(tabW, "%v\t%s\n", green(issue.duration), issue.issueId)
		total += issue.duration
	}

	_ = tabW.Flush()

	fmt.Fprintf(writer, "%s Total: %v\n", date.Format("1/2/06 (Mon)"), total)
	return nil
}

func getClient(url, username, password string) (*jira.Client, error) {
	client, err := jira.NewClient(nil, url)
	if err != nil {
		return nil, err
	}

	_, err = client.Authentication.AcquireSessionCookie(username, password)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func handleDate(dateParam string) (time.Time, error) {
	if dateParam == "" {
		if time.Now().Weekday() == time.Monday {
			dateParam = time.Now().AddDate(0, 0, -3).Format("2006-01-02")
		} else {
			dateParam = time.Now().AddDate(0, 0, -1).Format("2006-01-02")
		}
	} else {
		relativeDate, err := strconv.Atoi(dateParam)
		if err == nil {
			dateParam = time.Now().AddDate(0, 0, -relativeDate).Format("2006-01-02")
		}
	}

	return time.Parse("2006-01-02", dateParam)
}

func getDurations(client *jira.Client, username string, date time.Time) ([]issueData, error) {
	so := &jira.SearchOptions{
		StartAt:    0,
		MaxResults: 50,
		Fields:     []string{"*all"},
	}

	query := fmt.Sprintf("worklogDate = '%s' and worklogAuthor = %s", date.Format("2006-01-02"), username)

	issues, _, err := client.Issue.Search(query, so)
	if err != nil {
		return nil, fmt.Errorf("Unable to make search call: %v", err)
	}

	return parseDurations(client, issues, username, date)
}

func parseDurations(client *jira.Client, issues []jira.Issue, username string, date time.Time) ([]issueData, error) {
	durations := map[string]time.Duration{}
	for _, issue := range issues {
		blue := color.New(color.FgBlue).SprintFunc()
		issueKey := fmt.Sprintf("%s\t%s", blue(issue.Key), issue.Fields.Summary)
		durations[issueKey] = time.Duration(0)
		if issue.Fields != nil && issue.Fields.Worklog != nil {
			var worklogs []jira.WorklogRecord
			if issue.Fields.Worklog.MaxResults != issue.Fields.Worklog.Total {
				worklog, _, err := client.Issue.GetWorklogs(issue.Key)
				if err != nil {
					return nil, fmt.Errorf("Unable to make worklog call: %v", err)
				}

				worklogs = worklog.Worklogs
			} else {
				worklogs = issue.Fields.Worklog.Worklogs
			}

			for _, wl := range worklogs {
				if wl.Author.Name == username && time.Time(wl.Started).After(date) && time.Time(wl.Started).Before(date.Add(24*time.Hour)) {
					duration, _ := time.ParseDuration(fmt.Sprintf("%ds", wl.TimeSpentSeconds))
					durations[issueKey] += duration
				}
			}
		}
	}

	issueDatas := []issueData{}
	for issueId, duration := range durations {
		issueDatas = append(issueDatas, issueData{
			issueId:  issueId,
			duration: duration,
		})
	}

	return issueDatas, nil
}
